const express = require("express");
const mysql = require("mysql2");
const app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
const port = 3000;

const connection = mysql.createPool({
  host: "localhost",
  user: "libadm1475",
  database: "express_library",
  password: "TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv",
});

connection.getConnection((err) => {
  if (err instanceof Error) {
    console.log("getConnection err:", err);
    return;
  }
});

app.get("/", (req, res) => {
  res.send("Hello world !");
});

app.get("/books", (req, res) => {
  connection.query("SELECT * FROM book", (err, results) => {
    if (err) {
      res.status(500).send("Erreur lors de la récupération des livres");
    } else {
      res.json(results);
    }
  });
});

app.get("/books/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "SELECT * FROM book WHERE book_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération du livre");
      } else {
        res.json(results);
      }
    }
  );
});

app.post("/books", (req, res) => {
  const { title, author } = req.body;

  connection.execute(
    "INSERT INTO book (title, author) VALUES (?, ?)",
    [title, author],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout du livre");
      } else {
        res.status(201).send(`Livre ajouté avec l'ID ${results.insertId}`);
      }
    }
  );
});

app.put("/books/:id", (req, res) => {
  const { title, author } = req.body;
  const id = req.params.id;

  connection.execute(
    "UPDATE book SET title = ?, author = ? WHERE book_id = ? ",
    [title, author, id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour du livre");
      } else {
        res.send("Livre mis à jour");
      }
    }
  );
});

app.delete("/books/:id", (req, res) => {
  const id = req.params.id;
  connection.execute(
    "DELETE FROM book WHERE book_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la suppression du livre");
      } else {
        res.send("Livre supprimé.");
      }
    }
  );
});

app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});
